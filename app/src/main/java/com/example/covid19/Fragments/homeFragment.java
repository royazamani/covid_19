package com.example.covid19.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.covid19.R;
import com.example.covid19.recoverActivity;
import com.example.covid19.symptomsActivity;
import com.github.mikephil.charting.charts.LineChart;

import org.eazegraph.lib.charts.BarChart;
import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.PieModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;


public class homeFragment extends Fragment {


    /*
     "NewConfirmed": 100282,
    "TotalConfirmed": 1162857,
    "NewDeaths": 5658,
    "TotalDeaths": 63263,
    "NewRecovered": 15405,
    "TotalRecovered": 230845
     */
    View view;
    TextView NewConfirmed, TotalConfirmed, NewDeaths, TotalDeaths, NewRecovered, TotalRecovered;
    LottieAnimationView lottieAnimationView;
    RequestQueue requestQueue;
    String URL = "https://api.covid19api.com/summary";
    RelativeLayout relativeLayout;
    DecimalFormat format = new DecimalFormat("###,###");
    CardView cv_1 , cv_2 ;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        requestQueue = Volley.newRequestQueue(getContext());


        //cardview
        cv_1 = view.findViewById(R.id.cv_1);
        cv_2 = view.findViewById(R.id.cv_2);



        cv_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), symptomsActivity.class));
            }
        });

        cv_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), recoverActivity.class));
            }
        });




        relativeLayout = view.findViewById(R.id.main);
        NewConfirmed = view.findViewById(R.id.tvNewConfirmed);
        TotalConfirmed = view.findViewById(R.id.tvTotalConfirmed);
        NewDeaths = view.findViewById(R.id.tvNewDeaths);
        TotalDeaths = view.findViewById(R.id.tvTotalDeaths);
        NewRecovered = view.findViewById(R.id.tvNewRecovered);
        TotalRecovered = view.findViewById(R.id.tvTotalRecovered);
        lottieAnimationView = view.findViewById(R.id.lottieProgress);


        relativeLayout.setVisibility(View.INVISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                relativeLayout.setVisibility(View.VISIBLE);
                lottieAnimationView.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject globalJsonObject = jsonObject.getJSONObject("Global");

                    String NewConfirmed_str = globalJsonObject.getString("NewConfirmed");
                    String newcon = format.format(Integer.valueOf(NewConfirmed_str));
                    NewConfirmed.setText(newcon);

                    String TotalConfirmed_str = globalJsonObject.getString("TotalConfirmed");
                    String totalcon = format.format(Integer.valueOf(TotalConfirmed_str));
                    TotalConfirmed.setText(totalcon);

                    String NewDeaths_str = globalJsonObject.getString("NewDeaths");
                    String newdea = format.format(Integer.valueOf(NewDeaths_str));
                    NewDeaths.setText(newdea);

                    String TotalDeaths_str = globalJsonObject.getString("TotalDeaths");
                    String totaldea = format.format(Integer.valueOf(TotalDeaths_str));
                    TotalDeaths.setText(totaldea);

                    String NewRecovered_str = globalJsonObject.getString("NewRecovered");
                    String newrec = format.format(Integer.valueOf(NewRecovered_str));
                    NewRecovered.setText(newrec);

                    String TotalRecovered_str = globalJsonObject.getString("TotalRecovered");
                    String totalrec = format.format(Integer.valueOf(TotalRecovered_str));
                    TotalRecovered.setText(totalrec);





                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(stringRequest);


        return view;
    }
}