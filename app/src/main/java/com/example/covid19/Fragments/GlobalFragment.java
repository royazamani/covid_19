package com.example.covid19.Fragments;

import android.os.Bundle;

import androidx.constraintlayout.solver.widgets.analyzer.VerticalWidgetRun;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.UrlRewriter;
import com.android.volley.toolbox.Volley;
import com.example.covid19.Adapters.countryAdapter;
import com.example.covid19.Models.country;
import com.example.covid19.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


public class GlobalFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    EditText editText;
    LottieAnimationView lottieAnimationView;
    RequestQueue requestQueue;
    RelativeLayout relativeLayout;
    String Url = "https://api.covid19api.com/summary";
    List<country> data = new ArrayList<>();
    countryAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_global, container, false);

        requestQueue = Volley.newRequestQueue(getContext());


        recyclerView = view.findViewById(R.id.rv_global);
        editText = view.findViewById(R.id.et_search);
        lottieAnimationView = view.findViewById(R.id.lottieProgress);
        relativeLayout = view.findViewById(R.id.relativeglobal);

        relativeLayout.setVisibility(View.INVISIBLE);
        lottieAnimationView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        final StringRequest stringRequest = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    lottieAnimationView.setVisibility(View.GONE);
                    relativeLayout.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("Countries");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        /*
                              "Country": "ALA Aland Islands",
                              "NewConfirmed": 0,
                              "NewDeaths": 0,
                              "NewRecovered": 0,
                         */
                        String country = jsonObject1.getString("Country");
                        String NewConfirmed = jsonObject1.getString("NewConfirmed");
                        String NewRecovered = jsonObject1.getString("NewRecovered");
                        String NewDeaths = jsonObject1.getString("NewDeaths");
                        String TotalConfirmed=jsonObject1.getString("TotalConfirmed");
                        String TotalDeaths = jsonObject1.getString("TotalDeaths");
                        String TotalRecovered = jsonObject1.getString("TotalRecovered");

                        country country1 = new country();
                        country1.setCountry(country);
                        country1.setNewConfirmed(NewConfirmed);
                        country1.setNewRecovered(NewRecovered);
                        country1.setNewDeaths(NewDeaths);
                        country1.setTotalConfirmed(TotalConfirmed);
                        country1.setTotalDeaths(TotalDeaths);
                        country1.setTotalRecovered(TotalRecovered);

                        data.add(country1);
                        adapter = new countryAdapter(getContext(), data);
                        recyclerView.setAdapter(adapter);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        requestQueue.add(stringRequest);


        return view;
    }
}