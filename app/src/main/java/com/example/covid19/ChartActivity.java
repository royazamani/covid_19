package com.example.covid19;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.covid19.Adapters.countryAdapter;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.text.DecimalFormat;

public class ChartActivity extends AppCompatActivity   {
    TextView tv_chart_name ;
    Bundle bundle ;
    PieChart pieChart ;
    TextView NewConfirmed, TotalConfirmed, NewDeaths, TotalDeaths, NewRecovered, TotalRecovered;
    DecimalFormat format = new DecimalFormat("###,###");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        bundle = getIntent().getExtras();
        tv_chart_name = findViewById(R.id.tv_chart_name);
        pieChart = findViewById(R.id.piechart);
        NewConfirmed = findViewById(R.id.tvNewConfirmed);
        TotalConfirmed = findViewById(R.id.tvTotalConfirmed);
        NewDeaths = findViewById(R.id.tvNewDeaths);
        TotalDeaths = findViewById(R.id.tvTotalDeaths);
        NewRecovered = findViewById(R.id.tvNewRecovered);
        TotalRecovered = findViewById(R.id.tvTotalRecovered);



        NewConfirmed.setText(format.format(Integer.valueOf(bundle.getString("tv_confirmed_country"))));
        TotalConfirmed.setText(format.format(Integer.valueOf(bundle.getString("total_confirmed_country"))));
        NewDeaths.setText(format.format(Integer.valueOf(bundle.getString("tv_death_country"))));
        TotalDeaths.setText(format.format(Integer.valueOf(bundle.getString("total_death_country"))));
        NewRecovered.setText(format.format(Integer.valueOf(bundle.getString("tv_recovered_country"))));
        TotalRecovered.setText(format.format(Integer.valueOf(bundle.getString("total_recovered_country"))));


        tv_chart_name.setText(bundle.getString("tv_name_country"));

        pieChart.addPieSlice(new PieModel("New_confirmed", Integer.valueOf(bundle.getString("tv_confirmed_country")), Color.parseColor("#3B0935")));
        pieChart.addPieSlice(new PieModel("New_recovered", Integer.valueOf(bundle.getString("tv_recovered_country")), Color.parseColor("#C1F6C7")));
        pieChart.addPieSlice(new PieModel("New_death", Integer.valueOf(bundle.getString("tv_death_country")), Color.parseColor("#65500F")));
        pieChart.addPieSlice(new PieModel("Total_confirmed", Integer.valueOf(bundle.getString("total_confirmed_country")), Color.parseColor("#ED8A8C")));
        pieChart.addPieSlice(new PieModel("Total_recovered", Integer.valueOf(bundle.getString("total_recovered_country")), Color.parseColor("#751212")));
        pieChart.addPieSlice(new PieModel("Total_death", Integer.valueOf(bundle.getString("total_death_country")), Color.parseColor("#65168C")));

        pieChart.startAnimation();

    }

}