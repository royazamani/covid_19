package com.example.covid19.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19.ChartActivity;
import com.example.covid19.Interface.ItemClickListener;
import com.example.covid19.Models.country;
import com.example.covid19.R;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.List;

public class countryAdapter extends RecyclerView.Adapter<countryAdapter.MyViewHolder> implements Filterable {

    Context context ;
    List<country> data ;
    List<country> dataFilter;

    FilterCountries filterCountries ;

    public countryAdapter(Context context, List<country> data) {
        this.context = context;
        this.data = data;
        this.dataFilter = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        DecimalFormat decimalFormat = new DecimalFormat("###,###");

        holder.tv_name_country.setText(data.get(position).getCountry());
        holder.tv_confirmed_country.setText(decimalFormat.format(Integer.valueOf(data.get(position).getNewConfirmed())));
        holder.tv_recovered_country.setText(decimalFormat.format(Integer.valueOf(data.get(position).getNewRecovered())));
        holder.tv_death_country.setText(decimalFormat.format(Integer.valueOf(data.get(position).getNewDeaths())));
        holder.tv_total_con.setText(decimalFormat.format(Integer.valueOf(data.get(position).getTotalConfirmed())));
        holder.tv_total_rec.setText(decimalFormat.format(Integer.valueOf(data.get(position).getTotalRecovered())));
        holder.tv_total_dea.setText(decimalFormat.format(Integer.valueOf(data.get(position).getTotalDeaths())));


        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onclick(View view) {
                Intent intent = new Intent(context, ChartActivity.class);
                intent.putExtra("tv_name_country",data.get(position).getCountry());
                intent.putExtra("tv_confirmed_country",data.get(position).getNewConfirmed());
                intent.putExtra("tv_recovered_country",data.get(position).getNewRecovered());
                intent.putExtra("tv_death_country",data.get(position).getNewDeaths());
                intent.putExtra("total_confirmed_country",data.get(position).getTotalConfirmed());
                intent.putExtra("total_recovered_country",data.get(position).getTotalRecovered());
                intent.putExtra("total_death_country",data.get(position).getTotalDeaths());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public Filter getFilter() {
        if (filterCountries == null){
            filterCountries = new FilterCountries(this , dataFilter);
        }
        return filterCountries;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name_country,tv_confirmed_country,tv_recovered_country,tv_death_country , tv_total_con , tv_total_rec , tv_total_dea;

        ItemClickListener itemClickListener ;

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name_country = itemView.findViewById(R.id.tv_name_country);
            tv_confirmed_country = itemView.findViewById(R.id.tv_confirmed_country);
            tv_recovered_country = itemView.findViewById(R.id.tv_recovered_country);
            tv_death_country = itemView.findViewById(R.id.tv_death_country);
            tv_total_con = itemView.findViewById(R.id.tvTotalConfirmed);
            tv_total_rec = itemView.findViewById(R.id.tvTotalRecovered);
            tv_total_dea = itemView.findViewById(R.id.tvTotalDeaths);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onclick(view);
                }
            });



        }
    }

}
