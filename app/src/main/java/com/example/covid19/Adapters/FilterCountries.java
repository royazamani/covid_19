package com.example.covid19.Adapters;

import android.widget.Filter;

import com.example.covid19.Models.country;

import java.util.ArrayList;
import java.util.List;

public class FilterCountries extends Filter {
    countryAdapter countryAdapter ;
    List<country> data ;

    public FilterCountries(com.example.covid19.Adapters.countryAdapter countryAdapter, List<country> countries) {
        this.countryAdapter = countryAdapter;
        this.data = countries;
    }

    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {

        FilterResults filterResults = new FilterResults();

        if (charSequence.length()>0){
            charSequence = charSequence.toString().toUpperCase();
            List<country> countryList = new ArrayList<>();
            for (int i = 0; i <data.size() ; i++) {
                if (data.get(i).getCountry().toUpperCase().contains(charSequence)){
                    countryList.add(data.get(i));
                }

            }

            filterResults.values = countryList;
            filterResults.count = countryList.size();


        }else {
            filterResults.count = data.size();
            filterResults.values=data;

        }

        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

        countryAdapter.data = (List<country>) filterResults.values;
        countryAdapter.notifyDataSetChanged();

    }
}
